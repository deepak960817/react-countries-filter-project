import React, {Component} from 'react';
import DisplayCountries from './components/DisplayCountries';
import FilterCountries from './components/FilterCountries';
import Countries from './countries.json';
import './App.css';


class App extends Component {

  state = {
    DisplayCountries : Countries
  }
  
  handleFilter = (val) => {
    const searchtext = val.toLowerCase();
    if(searchtext !== '-- choose a region --')
    {
      const filteredCountries = Countries.filter((country) => {
      return(country.region.toLowerCase().includes(searchtext))
      })

    this.setState({DisplayCountries: filteredCountries});
    }
    else
    {
      this.setState({DisplayCountries: Countries});
    }
  }

  render() { 
    return (
      <div>
        <div style={{display: 'flex', justifyContent: 'center', padding: '2rem'}}>
        <FilterCountries textChange={this.handleFilter}/>
        </div>
        <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
          {this.state.DisplayCountries.map((country) => {
          return <DisplayCountries key={country['name']['official']} {...country} />
          })}
        </div>
      </div>
      
      
      
    )
  }
}
 
export default App;
