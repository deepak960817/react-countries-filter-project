import React from 'react';

function DisplayCountries (props) {

    return ( 
        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-around', width:'30%', padding: '2rem', margin: '1rem', backgroundColor: 'orange'}}>
            <p>{props.name.official} from {props.region}</p>
            <img src={props.flags.svg} width='50%' alt="" />  
        </div>
           
     );
}
 
export default DisplayCountries;
