import React, {Component} from 'react';

class FilterCountries extends Component {
    
    render() { 
        return (
            <select onChange={(e) => this.props.textChange(e.target.value)} className='filterBox' type="text">
                <option> -- Choose a Region -- </option>
                <option>Americas</option>
                <option>Asia</option>
                <option>Europe</option>
                <option>Oceania</option>
                <option>Africa</option>
                <option>Antarctic</option>
            </select>
     )
    }
}
 
export default FilterCountries;